# Reasonable data request

A static form (single HTML page) to write a templated email to the authors of
papers that promise code and data upon a "reasonable request".

The form starts from asking for a DOI, which then prefills paper-related form
fields with data from the CrossRef API. It uses a templating engine to generate
the email and provides a live preview. It has a "send" button that opens the
mailto: link and a copy to clipboard button for easy access to the generated
email.